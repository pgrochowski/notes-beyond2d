\documentclass[aps,prd,10pt,amsmath,amsfonts,onecolumn,nofootinbib,notitlepage]{revtex4-1}

\usepackage[utf8]{inputenc}
% \usepackage[OT4]{fontenc}

\usepackage{bm}
\usepackage{graphicx}
\usepackage{physics}
%\MakePerPage{footnote}

\usepackage{subcaption}

\usepackage{color}
\newcommand{\bl}[1]{\textcolor{blue}{\textbf{#1}}}

\usepackage{physics}
\usepackage[colorlinks]{hyperref}

\hypersetup{
	bookmarksnumbered,
	pdfstartview={FitH},
	citecolor={green},
	linkcolor={darkblue},
	urlcolor={darkblue},
	pdfpagemode={UseOutlines}}
\definecolor{darkgreen}{RGB}{20,100,20}
\definecolor{darkblue}{RGB}{0,0,130}
\definecolor{darkred}{rgb}{.8,0,0}

% Opus magnum shorthands
\newcommand{\hw}{\hbar \omega}
\newcommand{\kin}{\frac{\hbar^2 k^2}{2 m}}
\newcommand{\state}[1]{\left| \mathbf{k}, #1 \right\rangle }
\newcommand{\ef}{E_F}
\newcommand{\intk}[2]{\int_{#1}^{#2} \text{d} \mathbf{ k} \, \,}
\newcommand{\kf}[1]{{}_{#1}k_F}
\newcommand{\hwt}{\hbar^2 \omega^2}
\newcommand{\ndd}{n_{2D}}
\newcommand{\ntd}{n_{3D}}
\newcommand{\nz}{\widetilde{n}(z)}
\newcommand{\nddtd}{n_{2D3D}}
\newcommand{\di}{\text{d}}




\begin{document}

\title{2D Fermi gas coupled to a harmonic oscillator degree of freedom}

\author{Piotr T. Grochowski}
\email{piotr@cft.edu.pl}

\affiliation{Center for Theoretical Physics, Polish Academy of Sciences, Aleja Lotnik\'ow 32/46, 02-668 Warsaw, Poland}

\date{\today}

\maketitle

\section{Basic overview of the situation}

We derive an expression for Thomas-Fermi kinetic energy functional for two-dimensional non-interacting zero-temperature uniform polarized Fermi gas under the assumption that it has another degree of freedom, as it can occupy consecutive energy states of the harmonic oscillator. The aim of this treatment is to describe a Fermi gas that is strongly confined in one direction (perpendicular) by a harmonic potential, namely in quasi-2D situation. In the limit of very strong confinement, when the energy of excitation in the perpendicular direction $\hw$ is much higher than the Fermi energy of the gas $\ef$, we retrieve a description that is exactly two-dimensional. We consider the situation in which we allow the Fermi energy to exceed the perpendicular excitation energy but not too much in order not to destroy the assumption of low density of states in the perpendicular direction.

We start with semi-classical picture of two-dimensional Fermi gas. To calculate kinetic energy functional we firstly assume that consecutive fermions occupy consecutive energy levels described by two quantum numbers, $k_x$ and $k_y$, up to Fermi energy. We integrate over the $k$-space, effectively calculating the volume of 2D sphere of radius $k_F$. We then assume that every state resides in $1/(2\pi / L)^2$ fraction of the sphere's volume and this way we get the number of all particles $N$ and following that, density of particles, $n=N/L^2$. Then we similarly calculate the expectation value of the kinetic energy, $\kin$, getting its value as the function of $\ef$. We can now get rid of $\ef$ as we know its dependence on $n$. The results is proportional to $n^2$. We now proceed to perform this approach in the situation with an additional degree of freedom.

\section{Deriving the functional}
Now the one-particle state that we consider is described by three quantum numbers: $\state{j}$, where $j$ is a natural number. The energy of that state is
\begin{equation}
E_{\state{j}}=\kin+j\hw.
\end{equation}
Let us focus on how the fermions consecutively, with growing energy, occupy these states.

For energies from $0$ to $\hw$, we can only have states $\state{0}$, as there's not enough energy to excite the perpendicular direction. The volume of the $k$-space that is occupied is as follows:
\begin{equation}
\state{0} \longrightarrow 0 < \kin < \hw.
\end{equation}
When the energy is from $\hw$ to $2\hw$, a particle can either occupy a state with high momentum and still be in the perpendicular ground state or a state with low momentum and be in the first perpendicular excited state:
\begin{align}
&\state{0} \longrightarrow \hw < \kin < 2 \hw, \nonumber \\
&\state{1} \longrightarrow 0 < \kin < \hw.
\end{align}
When the energy is even higher, from $2 \hw$ to $3 \hw$, a particle can be in up to three situations -- $\state{0}$ with high momentum, $\state{2}$ with low momentum and $\state{1}$ with momentum which is between these two. In general, for the energy from $j \hw$ to $(j+1) \hw$ there are $j+1$ options:
\begin{align}
&\state{0} \longrightarrow j \hw < \kin < (j+1) \hw, \nonumber \\
&\state{1} \longrightarrow (j-1) \hw < \kin < j \hw, \nonumber \\
& \, \, \,... \nonumber \\
&\state{j} \longrightarrow 0 < \kin < \hw.
\end{align}
We have to take a special care for $l$ such that $l\hw < \ef < (l+1) \hw$. Again, we have $l$ options, but the limiting values of momentum are bounded by the Fermi energy:
\begin{align}
&\state{0} \longrightarrow l \hw < \kin < \ef - \hw, \nonumber \\
&\state{1} \longrightarrow (l-1) \hw < \kin < \ef - \hw, \nonumber \\
& \, \, \,... \nonumber \\
&\state{l} \longrightarrow 0 < \kin < \ef - l \hw.
\end{align}

As we now see how the particles occupy the single-particle states we can proceed to construct a method to properly integrate over the whole $k$-space. Let $\Omega$ be a volume in the space of quantum numbers $(\mathbf{k},j)$ in which the states are occupied. We introduce three further definitions:
\begin{align}
k_j&=\sqrt{\frac{2 m}{\hbar^2} j \hw}, \\
{}_jk_F&=\sqrt{\frac{2 m}{\hbar^2}\left(\ef -j\hw \right) } ,\\
\Lambda&=\left\lbrace \mathbf{k}: \, k<k_j,\,k>k_i \right\rbrace ,\nonumber \\
\intk{\Lambda}{}&=\intk{k_i}{k_j}.
\end{align}
Then the integration over $\Omega$ is given by:
\begin{align}
\int_{\Omega} = & \intk{k_0}{k_1} + \left( \intk{k_1}{k_2} + \intk{k_0}{k_1}  \right) + \left( \intk{k_2}{k_3} + \intk{k_1}{k_2} + \intk{k_0}{k_1}  \right) +  \nonumber \\
& ... + \left( \intk{k_l}{{}_0k_F} + \intk{k_{l-1}}{{}_1k_F} + ... + \intk{k_{0}}{{}_lk_F}  \right)= \nonumber \\
&  \intk{k_0}{{}_0k_F} + \intk{k_{0}}{{}_1k_F} + ... + \intk{k_{0}}{{}_lk_F}.
\end{align}
Now we can calculate both $n$ and $\epsilon_K$ as $n=\frac{1}{(2 \pi)^2} \int_{\Omega} 1 $ and $\epsilon_K=\frac{1}{(2 \pi)^2} \int_{\Omega} \kin $:
\begin{align}
n=&\frac{1}{(2 \pi)^2} \int_{\Omega} 1 = \frac{1}{(2 \pi)^2} \pi \left( \kf{0}^2-k_0^2 + \kf{1}^2 - k_0^2 + ... + \kf{l}^2 - k_0^2   \right) =  \nonumber \\
=& \frac{1}{4 \pi} \frac{2 m}{\hbar^2} \left( \ef + \ef - \hw + ... + \ef - l \hw \right) = \frac{m}{2 \pi \hbar^2} \left( \left( l+1 \right) \ef - \frac{1}{2} \hw l\left( l+1 \right)   \right).
\end{align}
We can extract Fermi energy from this equation:
\begin{align}\label{fermienergy}
\ef = \frac{1}{l+1} \left( \frac{2 \pi \hbar^2}{m} n + \frac{1}{2} \hw l\left( l+1 \right) \right).
\end{align}
As for the kinetic energy:
\begin{align} \label{kinetic}
\epsilon_K=&\frac{1}{(2 \pi)^2} \int_{\Omega} \kin = \frac{\hbar^2}{8 \pi^2 m} 2 \pi \frac{1}{4} \left( \kf{0}^4-k_0^4 + \kf{1}^4 - k_0^4 + ... + \kf{l}^4 - k_0^4 \right) = \nonumber \\
& \frac{\hbar^2}{16 \pi m} \frac{4 m^2}{\hbar^4} \left[\ef^2 + (\ef - \hw)^2 + ... + (\ef - l\hw)^2 \right]  = \nonumber \\
& \frac{m}{4 \pi \hbar^2} \left[ (l+1) \ef^2 + \frac{1}{6} \hwt l(l+1)(2l+1) - \ef \hw l(l+1) \right].
\end{align}
We can now insert \eqref{fermienergy} into \eqref{kinetic} and get the functional:
\begin{align} \label{kineticq}
\epsilon_K[n]=\frac{\pi \hbar^2}{(l+1)m}n^2 + \frac{m \omega^2}{48 \pi} l(l+1)(l+2),
\end{align}
which in case $l=0$ simplifies into standard 2D kinetic energy of the Fermi gas $\epsilon_K^{2D}[n]=\frac{\pi \hbar^2}{m}n^2$.

We are now interested in how it affects pseudo-Schr\"odinger equation. The functional goes into it as a functional derivative:
\begin{align} \label{kina}
\frac{\delta}{\delta n(\mathbf{r})} \int \text{d}\mathbf{r} \, \epsilon_K[n(\mathbf{r})] = \frac{2 \pi \hbar^2}{(l+1)m}n(\mathbf{r}),
\end{align}
where $l$ has to be self-consistently calculated as
\begin{align}
\left \lfloor{\frac{\ef}{\hw}}\right \rfloor=\left \lfloor{\frac{2 \pi \hbar^2}{(l+1)m \hw}n +\frac{1}{2}l}\right \rfloor=l,
\end{align}
where $\left \lfloor \cdot \right \rfloor $ means taking integer part of the number.

\section{3D correspondence}
Now we aim to construct a mapping from our two-dimensional description to a real, three-dimensional density $n_{3D} (x,y,z)$. In the LDA we want to have:
\begin{align}
n_{2D} (x,y) \longrightarrow n_{2D} (x,y) \widetilde{n}(z,n_{2D}) \equiv \widetilde{n}_{3D}(x,y,z) \approx n_{3D} (x,y,z).
\end{align}
It is meant to work when the confinement in the perpendicular direction is strong and the effective potential in this direction is the harmonic potential. Locally, we have $n_{2D} = const$ and if we assume that there are $N$ atoms per surface unit $S$, we want $\widetilde{n}(z,n_{2D})=\widetilde{n}(z)$, such that:
\begin{align}
\int \text{d}x \, \text{d}y \, \text{d}z \, n_{2D} \widetilde{n}(z) = N,
\end{align} 
where the integration is over $S$ and the whole $z$-direction. Note that $\widetilde{n}(z)$ is an auxiliary object and it is normalized, $\int \widetilde{n}(z) \text{d}z =1$. Let us construct it in such a way that it satisfies above conditions. Firstly, let us recall densities of the eigensolutions of the harmonic oscillator potential:
\begin{align}
n_j (z) = \frac{1}{2^j j!} \sqrt{\frac{m \omega}{\pi \hbar}} e^{-\frac{m \omega z^2}{\hbar}} H_j^2\left( \sqrt{\frac{m \omega}{\hbar}} z \right),
\end{align}
where $H_j(x)$ is the j-th physicists' Hermite polynomial. So, we can write:
\begin{align}
N=&\ndd S = S \frac{1}{(2 \pi)^2} \int_{\Omega} 1 = \frac{S}{(2 \pi)^2} \sum_{j=0}^l \intk{k_0}{\kf{j}} = \frac{S}{(2 \pi)^2} \sum_{j=0}^l \int_{-\infty}^{\infty} n_j(z) \text{d}z \intk{k_0}{\kf{j}} = \nonumber \\
& S \ndd \frac{1}{\ndd} \int_{-\infty}^{\infty} \text{d}z \frac{1}{(2 \pi)^2} \sum_{j=0}^l \left\lbrace  n_j(z) \intk{k_0}{\kf{j}} \right\rbrace.
\end{align}
Therefore, we extract the expression for the density in $z$-direction:
\begin{align}\label{nz}
\nz = \frac{\sum_{j=0}^l n_j(z) \intk{k_0}{\kf{j}} }{\sum_{j=0}^l  \intk{k_0}{\kf{j}}} = \frac{\sum_{j=0}^l \left( \ef - j \hw \right) n_j(z) }{ \left( l+1 \right) \ef - \hw l \left( l+1 \right)/2  }.
\end{align}
Note that this is only an Ansatz which validity will be further discussed. Other choices satisfying earlier properties are possible. It can be treated as a function of two-dimensional density $\widetilde{n}=\widetilde{n}(z,\ndd)$ as local Fermi energy in LDA is a one-to-one function of the density:
\begin{align}
\nz = \frac{m}{2 \pi \hbar^2} \frac{1}{\ndd} \sum_{j=0}^l  \left\lbrace \frac{1}{l+1} \left( \frac{2 \pi \hbar^2}{m} \ndd + \frac{1}{2} \hw l\left( l+1 \right) \right) - j \hw \right\rbrace n_j(z).
\end{align}
That leaves with expression for constructed three-dimensional density as a function of n:
\begin{align}
\widetilde{n}_{3D} (\ndd,z) = \frac{m}{2 \pi \hbar^2} \sum_{j=0}^l  \left\lbrace \frac{1}{l+1} \left( \frac{2 \pi \hbar^2}{m} \ndd + \frac{1}{2} \hw l\left( l+1 \right) \right) - j \hw \right\rbrace n_j(z).
\end{align}

Now we move to the mapping the densities the other way -- we want to extract $\ndd$ from $\ntd$. We define
\begin{align} \label{n2dmap}
\nddtd (x,y) \equiv \int_{-\infty}^{\infty} \ntd (x,y,z) \text{d}z.
\end{align}
It is supposed to satisfy
\begin{align}
\widetilde{n}_{3D}(x,y,z) \approx  \nddtd (x,y) \, \widetilde{n}(z,\nddtd).
\end{align}
We later check validity of this Ansatz with the ideal gas.

In the two-dimensional kinetic energy functional \eqref{kineticq} we did not account for the perpendicular degree of freedom as we did not calculate the expectation value of the kinetic energy operator associated with the $z$-direction. As we now have the density profile in this direction, we can calculate it in the position space. We can write down
\begin{align}
 E_{K,z}=\int \di x \di y \di z \, \frac{p_z^2}{2 m} \left( \nz \right) \widetilde{n}_{3D}(x,y,z) = \int \di x \di y \, \ndd (x,y) \int \di z \, \frac{p_z^2}{2 m} \left( \nz \right),
\end{align}
from which we arrive at the functional
\begin{align}
\epsilon_{K,z}=\ndd \int \di z \, \frac{p_z^2}{2 m} \left( \nz \right).
\end{align}
We have to evaluate the kinetic energy operator on the perpendicular state. It is a linear combination of the harmonic oscillator eigensolutions, so average has to be calculated on each of them and then combined accordingly. We know that the following identity holds for the harmonic oscillator:
\begin{align}
 \left\langle \frac{p_z^2}{2 m} \right\rangle_j = \left\langle \frac{1}{2} m \omega^2 z^2 \right\rangle_j = \frac{1}{2} m \omega^2 \int_{-\infty}^{\infty} z^2 n_j(z) \di z =  \frac{2j+1}{4} \hw    ,
\end{align}
so we can explicitly express the functional as:
\begin{align}
\epsilon_{K,z} = \ndd \frac{\sum_{j=0}^l \left( \ef - j \hw\right) \hw \frac{2j+1}{4} }{\left( l+1 \right) \ef - \hw l \left( l+1 \right)/2}.
\end{align}
If we express Fermi energy as a function of two-dimensional density $\ndd = n$, we arrive at
\begin{align}
\epsilon_{K,z} = \frac{m \omega}{8 \pi \hbar} \sum_{j=0}^l \left( 2j+1 \right) \left\lbrace \frac{1}{l+1} \left( \frac{2 \pi \hbar^2}{m} n + \frac{1}{2} \hw l\left( l+1 \right) \right) - j \hw \right\rbrace. 
\end{align}
The full kinetic energy functional yields:
\begin{align}\label{fullk}
\epsilon_K[n]=\frac{\pi \hbar^2}{(l+1)m}n^2 + \frac{m \omega^2}{48 \pi} l(l+1)(l+2) +  \frac{m \omega}{8 \pi \hbar} \sum_{j=0}^l \left( 2j+1 \right) \left\lbrace \frac{1}{l+1} \left( \frac{2 \pi \hbar^2}{m} n + \frac{1}{2} \hw l\left( l+1 \right) \right) - j \hw \right\rbrace.
\end{align}
It apparently doesn't reduce to purely two-dimensional case, as there is an additional term, that depends linearly on the density. In the case in which in the whole gas $l$ is constant (for pure two-dimensional case it means $l=0$) this linear term gets integrated into some constant, which is usually dropped, as it only changes the way we calculate the full energy. In our case it is indeed a dynamical variable, as this linear term can have different coefficients across the gas.

The functional derivative of the functional that goes into the time evolution equation is then:
\begin{align} \label{kina2}
	\frac{\delta}{\delta n(\mathbf{r})} \int \text{d}\mathbf{r} \, \epsilon_K[n(\mathbf{r})] = \frac{2 \pi \hbar^2}{(l+1)m}n(\mathbf{r}) + \frac{\hw (l+1)}{4},
\end{align}
where $l$ has to be self-consistently calculated as
\begin{align}
\left \lfloor{\frac{2 \pi \hbar^2}{(l+1)m \hw}n +\frac{1}{2}l}\right \rfloor=l.
\end{align}

\section{Testing the functional}
We need to compare Ansatzen made with some situation that will work as a benchmark for them. We will consider an ideal gas in a harmonic trap, characterized by perpendicular confinement $\omega_z$ and symmetric trap in $xy$ plane, given by the frequency $\omega$. We will work in Thomas-Fermi approximation for the gas density and its kinetic energy. In 3D limit we will use three-dimensional density profile and the accumulated two-dimensional density, defined as in \eqref{n2dmap}:
\begin{align}
\ntd (\rho,z) &= \frac{1}{6 \pi^2} \left( \frac{2 m}{\hbar^2} \right)^\frac{3}{2} \left[ \left( 6 \omega_z \omega^2 \hbar^3 N \right)^{1/3} - \frac{1}{2} m \omega^2 \rho^2 - \frac{1}{2} m \omega_z^2 z^2   \right]^{\frac{3}{2}}, \\
\nddtd (\rho) &= \frac{m}{4 \pi \omega_z \hbar^{3/2}} \left[ \left( 6 \omega_z \omega^2 \hbar^3 N \right)^{1/3} - \frac{1}{2} m \omega^2 \rho^2    \right]^2.
\end{align}
The kinetic energy is calculated as
\begin{align}
T_{3D}=\frac{6^{5/3} \hbar^2 \pi^{4/3}}{20 m} \int \text{d}\mathbf{r} \, \ntd^{5/3}(\mathbf{r})=2^{-8/3} 3^{4/3} \hbar \omega^{2/3} \omega_z^{1/3} N^{4/3}.
\end{align}
For two-dimensional densities we use the definitions from the last section:
\begin{align}
\ndd (\rho) = \frac{m}{2 \pi \hbar^2} \left( l+1 \right) \left( \mu - \frac{1}{2} m \omega^2 \rho^2 - \frac{1}{2} \hbar \omega_z l  \right), \, \, \, \,  \, l=\left \lfloor{ \frac{\mu - \frac{1}{2} m \omega^2 \rho^2}{\hbar \omega_z}  }\right \rfloor.
\end{align}
$\widetilde{n}_{3D}(\rho,z) = n_{2D} (\rho) \widetilde{n}(z,\rho)$ is calculated from the prescription given in \eqref{nz} and the kinetic energy is calculated through functional \eqref{fullk}.

In Fig.~\ref{densnum} we show the comparison of the density profiles in each cases for constant trap ratio and changing number of particles. In Fig.~\ref{densfreq} similar comparison is presented, but with constant number of atoms and trap ratio as a variable. In Figs.~\ref{kins1},~\ref{kins2} we compare the full kinetic energy in analogous situations.

\begin{figure*}[h!tbp]
	\centering
	    \begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.25\linewidth]{3.pdf}
		\includegraphics[width=0.25\linewidth]{4.pdf}
	    \end{subfigure}
        
    	 \begin{subfigure}[b]{\textwidth}
	   \includegraphics[width=0.25\linewidth]{7.pdf}
	   \includegraphics[width=0.25\linewidth]{8.pdf}
      \end{subfigure}
 	
    	 \begin{subfigure}[b]{\textwidth}
	\includegraphics[width=0.25\linewidth]{11.pdf}
	\includegraphics[width=0.25\linewidth]{12.pdf}
		\end{subfigure}	  		       
  \caption{The comparison of the density profiles for two- and three-dimensional functionals in case of symmetric trap and different numbers of particles. Very good agreement is seen already for no less than 10-20 atoms. For very large numbers of atoms density profiles are the same up to couple of significant digits. \label{densnum}}
\end{figure*}

\begin{figure*}[h!tbp]
	\centering
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.25\linewidth]{1a.pdf}
		\includegraphics[width=0.25\linewidth]{2a.pdf}
	\end{subfigure}
	
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.25\linewidth]{7a.pdf}
		\includegraphics[width=0.25\linewidth]{8a.pdf}
	\end{subfigure}
		
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.25\linewidth]{11a.pdf}
		\includegraphics[width=0.25\linewidth]{12a.pdf}
	\end{subfigure}	  		       
  \caption{The comparison of the density profiles for two- and three-dimensional functionals in case of equal number of particles $N=5000$ and different trap ratios. Quite good agreement is seen already in deep two-dimensional regime, with maximal $l$ equal 3 or so. Deep in three-dimensional regime density profiles are the same up to couple of significant digits. \label{densfreq}}	
\end{figure*}
\begin{figure*}[h!tbp]
	\centering
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.4\linewidth]{w1.pdf}
		\includegraphics[width=0.42\linewidth]{w2.pdf}
	\end{subfigure}
  \caption{The comparison of the full kinetic energies for two- and three-dimensional functionals in case of equal number of particles $N=10000$ and different trap ratios. Good agreement is seen even in the two-dimensional regime (up to 5$\%$) \label{kins1}}	
\end{figure*}
\begin{figure*}[t]
	\centering
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.4\linewidth]{w3.pdf}
		\includegraphics[width=0.42\linewidth]{w4.pdf}
	\end{subfigure}
	\caption{The comparison of the full kinetic energies for two- and three-dimensional functionals in case of symmetric trap and different numbers of particles. Good agreement is seen even for small numbers of atoms (up to 5$\%$) \label{kins2}}	
\end{figure*}

\end{document}